# Custom CA SSL Server

This repository builds a docker image that can be used to test out connecting to a TLS server with a self
signed CA certificate. It was created to be used by
https://gitlab.com/gitlab-org/security-products/tests/custom-ca in support of
https://gitlab.com/gitlab-org/gitlab/issues/11797

## Hostname

<https://ssl-test/> is the host that the self signed certificate is setup to host.

## Public CA CRT

The ca.crt file in the root of this repo can be added to your bundle of trusted CAs for your system before
connecting to this server as a test for certificates signed by custom CA certificates.
