FROM nginx

EXPOSE 443

COPY ca.crt .
COPY ssl-test.bundle.crt /etc/nginx/
COPY ssl-test.key /etc/nginx/
COPY nginx.conf /etc/nginx/nginx.conf
